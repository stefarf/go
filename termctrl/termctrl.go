package termctrl

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
)

var (
	wg          sync.WaitGroup
	Terminated  bool
	ChTerminate = make(chan bool)
)

func init() {
	// Disable interrupt signal
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		fmt.Println("Interrupt signal")
		Terminated = true
		close(ChTerminate)
	}()
}

func Run(job func()) bool {
	if Terminated {
		return false
	}
	wg.Add(1)
	defer wg.Done()
	job()
	return true
}

func Wait() {
	<-ChTerminate
	wg.Wait()
}
