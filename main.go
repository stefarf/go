package main

import "gitlab.com/stefarf/go/iferr"

func main() {
	iferr.Panic(nil)
	iferr.Print(nil)
	iferr.Println(nil)
}
