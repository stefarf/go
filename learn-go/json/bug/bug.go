package main

import (
	"encoding/json"
	"fmt"
	"github.com/kr/pretty"
	"gitlab.com/stefarf/go/iferr"
	"google.golang.org/api/androidmanagement/v1"
)

type (
	sample struct {
		androidmanagement.Policy
		Age int
	}
)

func main() {
	v1 := sample{
		Policy: androidmanagement.Policy{
			AddUserDisabled: true,
			Name:            "Waoo",
		},
		Age: 17,
	}
	bug(v1)

	v2 := []*sample{&v1}
	bug(v2)
}

func bug(v interface{}) {
	pretty.Println(v)
	b, err := json.Marshal(v)
	iferr.Panic(err)
	fmt.Println(string(b))
}
