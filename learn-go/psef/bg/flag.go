package main

import (
	"flag"
)

var (
	fStart   = flag.Bool("start", false, "")
	fStop    = flag.Bool("stop", false, "")
	fRestart = flag.Bool("restart", false, "")
	fList    = flag.Bool("list", false, "")
)

func init() {
	flag.Parse()
}
