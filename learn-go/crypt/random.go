package crypt

import (
	"crypto/rand"
	"encoding/base64"
	"gitlab.com/stefarf/go/iferr"
)

func GenerateRandomBytes(numBytes int) (key []byte) {
	key = make([]byte, numBytes)
	_, err := rand.Read(key)
	iferr.Panic(err)
	return
}

func GenerateRandomString(numBytes int) string {
	return base64.StdEncoding.EncodeToString(GenerateRandomBytes(numBytes))
}

func GenerateRandomStringURLSafe(numBytes int) string {
	return base64.URLEncoding.EncodeToString(GenerateRandomBytes(numBytes))
}
