package main

import (
	"io/ioutil"

	"github.com/kr/pretty"
	"gitlab.com/stefarf/go/iferr"
	"gopkg.in/yaml.v2"
)

var (
	data struct {
		Name    string
		Age     int
		Married bool
		Map     map[string]interface{}
	}
)

func main() {
	b, err := ioutil.ReadFile("data.yaml")
	iferr.Exit(err, "")
	iferr.Exit(yaml.Unmarshal(b, &data), "")

	pretty.Println(&data)
}
