package main

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"log"
	"sync"
)

const (
	mqttHost     = "mqtt.vostra.xyz"
	mqttPort     = 1883
	mqttClientID = "arief2"
	mqttUsername = "arief2"
	mqttPassword = "Vostra123"
)

var (
	wg sync.WaitGroup
)

func connect() {
	opts := mqtt.NewClientOptions().AddBroker(
		fmt.Sprintf("tcp://%s:%d", mqttHost, mqttPort))
	opts.ClientID = mqttClientID
	opts.Username = mqttUsername
	opts.Password = mqttPassword
	opts.SetDefaultPublishHandler(func(cli mqtt.Client, msg mqtt.Message) {

	})

	opts.OnConnect = func(cli mqtt.Client) {
		fmt.Println("Connected")
		cli.Publish("test/gsm01/request", 2, false, "AT+CFSINIT")
	}

	cli := mqtt.NewClient(opts)
	if token := cli.Connect(); token.Wait() && token.Error() != nil {
		log.Println(token.Error())
	} else {
		cli.Subscribe("test/gsm01/response", 0, func(cli mqtt.Client, msg mqtt.Message) {
			fmt.Println("Response:")
			fmt.Println(string(msg.Payload()))
		})
		fmt.Println("Subscribed")
	}
}

func main() {
	wg.Add(1)
	connect()
	wg.Wait()
}
