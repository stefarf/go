## from-c:
Prototype to create dll from c that is able to be loaded by MT4.

## from-go:
Prototype to create dll to be loaded by MT4 directly from go.

## from-go2:
Prototype to create exe that import go function.

## from-go3:
Prototype to create dll to be loaded by MT4 from go indirectly via c.