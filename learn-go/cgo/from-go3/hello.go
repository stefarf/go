package main

import "C"
import "fmt"

//export HelloGo
func HelloGo() C.int {
	return 123
}

//export PrintHello
func PrintHello() {
	fmt.Println("Hello from Go")
}

func main() {}
