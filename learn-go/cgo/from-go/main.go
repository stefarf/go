package main

import "C"
import "io/ioutil"
import "encoding/json"

//export Hello
func Hello() C.int {
	return 111
}

//export PassArray
func PassArray(op []C.double) C.int {
	var data struct {
		Op1 float64
		Op2 float64
	}
	data.Op1 = float64(op[0])
	data.Op2 = float64(op[1])

	var rst int = -1

	b, err := json.Marshal(data)
	if err != nil {
		return C.int(rst)
	}

	err = ioutil.WriteFile("e:/tes.txt", b, 0777)
	if err != nil {
		return C.int(rst)
	}

	rst = 777
	return C.int(rst)
}

func main() {}
