# Step by step:

1.  Set environment for the credentials:
    ```bash
    . setenv.sh
    ```
2.  Generate the mp3:
    ```bash
    ./genmp3.sh notif "Anda mendapatkan notifikasi. Silahkan periksa."
    ```