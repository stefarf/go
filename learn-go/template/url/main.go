package main

import (
	"devx/iferr"
	"html/template"
	"os"
)

func main() {
	tpl, err := template.New("").Parse(`<div onclick="loadMain(this, {{.URL}} )"></div>`)
	iferr.Panic(err)
	tpl.Execute(os.Stdout, map[string]interface{}{
		"URL": template.URL("/todo/hai"),
	})
}
