#!/usr/bin/env bash
docker system prune -f
docker run -it --rm \
    -v "$PWD/src":/go/src \
    -w /go/src \
    golang
