# STEPS

1. Run `run-compiler.sh`.
2. Maintain the container open/running as long as you are experimenting (coding).
   The first invocation of go run/build is slow.


# NOTES

Though the `github.com/mattn/go-sqlite3` package suggest to enable the cgo in
order to compile/build:

```bash
# It works ...
CGO_ENABLED=1 go build -o sqlite-linux
```

but after experimenting it seems the explicit `CGO_ENABLED=1` is not needed:

```bash
# It works too ...
go run main.go
go build -o sqlite-linux
```