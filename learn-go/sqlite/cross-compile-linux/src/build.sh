#!/usr/bin/env bash
go get github.com/mattn/go-sqlite3
go run main.go
go build -o sqlite-linux
./sqlite-linux

# CGO is not needed ???
#CGO_ENABLED=1 go build -o sqlite-linux
