package something

type (
	Something interface {
		DoThis()
		DoThat()
	}
)
