# Reference

https://blog.gopheracademy.com/advent-2017/parsing-with-antlr4-and-go/

# Steps

```bash
# 1. Generate parser

#alias antlr='java -jar /usr/local/lib/antlr-4.7.1-complete.jar'
#antlr -Dlanguage=Go -o parser Calc.g4
java -jar /usr/local/lib/antlr-4.7.1-complete.jar -Dlanguage=Go -o parser Calc.g4

# 2. Execute examples

go run ex1.go
go run ex2.go
go run ex3.go
```