# Steps

To make it available in command line, copy script to `/usr/local/bin`:
```bash
chmod 755 *.sh
cp antlr-go.sh /usr/local/bin/antlr-go
```

Instead of `./antlr-go.sh -o parser Calc.g4` to generate the parser, use:
```bash
antlr-go -o parser Calc.g4
```