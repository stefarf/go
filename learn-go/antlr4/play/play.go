package main

import (
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/stefarf/go/learn-go/antlr4/play/parser"
)

func main() {
	input := "1 + 2 * 3"

	// Setup the input
	is := antlr.NewInputStream(input)

	// Create the Lexer
	lexer := parser.NewCalcLexer(is)
	stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)

	// Create the Parser
	p := parser.NewCalcParser(stream)

	// Finally parse the expression (by walking the tree)
	var listener parser.BaseCalcListener
	antlr.ParseTreeWalkerDefault.Walk(&listener, p.Start())
}
