package steps

import (
	"fmt"
)

type (
	Compiled []Mnemonic
)

var (
	codePrint = map[command]func(idx int, mne Mnemonic){
		cmdPopParamsCallAndSave: func(idx int, mne Mnemonic) {
			params := mne.i
			funcName := mne.s
			ids := mne.a
			fmt.Printf("%d: pop %d params, call %s() and save result to %v\n",
				idx, params, funcName, ids)
		},
		cmdPopParamsCallAndPush: func(idx int, mne Mnemonic) {
			params := mne.i
			funcName := mne.s
			fmt.Printf("%d: pop %d params, call %s() and push result\n",
				idx, params, funcName)
		},
		cmdPop2OperandAndPush: func(idx int, mne Mnemonic) {
			op := mne.s
			fmt.Printf("%d: pop 2, do operand '%s' and push result\n", idx, op)
		},
		cmdPushID: func(idx int, mne Mnemonic) {
			id := mne.s
			fmt.Printf("%d: push ID '%s'\n", idx, id)
		},
		cmdPushString: func(idx int, mne Mnemonic) {
			str := mne.s
			fmt.Printf("%d: push string '%s'\n", idx, str)
		},
		cmdPushNumber: func(idx int, mne Mnemonic) {
			num := mne.s
			fmt.Printf("%d: push number '%s'\n", idx, num)
		},
		cmdPopAndJumpIfFalse: func(idx int, mne Mnemonic) {
			label := mne.i
			fmt.Printf("%d: pop and jump to %d if false\n", idx, label)
		},
		cmdJump: func(idx int, mne Mnemonic) {
			label := mne.i
			fmt.Printf("%d: jump to %d\n", idx, label)
		},
		cmdPopAndAssign: func(idx int, mne Mnemonic) {
			id := mne.s
			fmt.Printf("%d: pop and assign '%s'\n", idx, id)
		},
		cmdPopAndPushParam: func(idx int, mne Mnemonic) {
			fmt.Printf("%d: pop and push param\n", idx)
		},
	}
)

func (com *Compiled) Print() {
	for idx, mne := range *com {
		codePrint[mne.cmd](idx, mne)
	}
}
