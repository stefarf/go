package steps

import (
	"bufio"
	"log"
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/stefarf/go/learn-go/antlr4/steps/parser"
)

type (
	stepsCompiler struct {
		parser.BaseStepsListener

		paramsStack []int

		labelLast  int
		labelStack []int

		elseStack []bool

		compiled Compiled
		addr     map[string]int
		pass2    []int

		syntaxError bool
	}

	Mnemonic struct {
		cmd command

		s string
		i int
		a []string
	}
)

type command byte

const (
	cmdPopParamsCallAndSave command = iota
	cmdPopParamsCallAndPush
	cmdPop2OperandAndPush
	cmdPushID
	cmdPushString
	cmdPushNumber
	cmdPopAndJumpIfFalse
	cmdJump
	cmdPopAndAssign
	cmdPopAndPushParam
)

var (
	DebugCompile = false
	errCompile   = errors.New("error compile")
)

// else

func (sc *stepsCompiler) pushElse()     { sc.elseStack = append(sc.elseStack, false) }
func (sc *stepsCompiler) popElse()      { sc.elseStack = sc.elseStack[:len(sc.elseStack)-1] }
func (sc *stepsCompiler) getElse() bool { return sc.elseStack[len(sc.elseStack)-1] }
func (sc *stepsCompiler) setElse()      { sc.elseStack[len(sc.elseStack)-1] = true }

// label

func (sc *stepsCompiler) pushLabel() {
	sc.labelStack = append(sc.labelStack, sc.labelLast)
	sc.labelLast++
}
func (sc *stepsCompiler) popLabel()     { sc.labelStack = sc.labelStack[:len(sc.labelStack)-1] }
func (sc *stepsCompiler) getLabel() int { return sc.labelStack[len(sc.labelStack)-1] }

// params

func (sc *stepsCompiler) pushParams() { sc.paramsStack = append(sc.paramsStack, 0) }
func (sc *stepsCompiler) incParams() {
	sc.paramsStack[len(sc.paramsStack)-1] = sc.paramsStack[len(sc.paramsStack)-1] + 1
}
func (sc *stepsCompiler) popParams() (params int) {
	params = sc.paramsStack[len(sc.paramsStack)-1]
	sc.paramsStack = sc.paramsStack[:len(sc.paramsStack)-1]
	return
}

//

func (sc *stepsCompiler) addMnemonic(m Mnemonic) {
	sc.compiled = append(sc.compiled, m)
}

func (sc *stepsCompiler) addPass2() {
	sc.pass2 = append(sc.pass2, len(sc.compiled))
}

func (sc *stepsCompiler) setAddr(label string) {
	sc.addr[label] = len(sc.compiled)
}

func (sc *stepsCompiler) compile(code string) error {
	if !DebugCompile {
		log.Printf("COMPILE:\n%s", code)
	}

	// Setup the input
	is := antlr.NewInputStream(code)

	// Create the Lexer
	lexer := parser.NewStepsLexer(is)
	stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)

	// Create the Parser
	p := parser.NewStepsParser(stream)
	p.RemoveErrorListeners()
	p.AddErrorListener(sc)

	// Finally parse the expression (by walking the tree)
	antlr.ParseTreeWalkerDefault.Walk(sc, p.Start())

	if sc.syntaxError {
		sc.compiled = nil
		return errCompile
	}

	// Do pass 2
	for _, idx := range sc.pass2 {
		sc.compiled[idx].i = sc.addr[sc.compiled[idx].s]
		sc.compiled[idx].s = ""
	}

	return nil
}

func Compile(code string) (*Compiled, error) {
	l := &stepsCompiler{addr: map[string]int{}}
	err := l.compile(code)
	if err != nil {
		return nil, err
	}
	return &l.compiled, nil
}

func toString(s string) string {
	out := make([]byte, 0, len(s)-2)

	rd := bufio.NewReader(strings.NewReader(s[1 : len(s)-1]))
	for {
		b, err := rd.ReadByte()
		if err != nil {
			break
		}
		if b == '\\' {
			b, err = rd.ReadByte()
			if err != nil {
				break
			}
			switch b {
			case 't':
				out = append(out, '\t')
			case 'n':
				out = append(out, '\n')
			case '\\':
				out = append(out, '\\')
			default:
				out = append(out, '\\', b)
			}
		} else {
			out = append(out, b)
		}
	}

	return string(out)
}
