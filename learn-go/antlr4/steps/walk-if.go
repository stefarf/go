package steps

import (
	"fmt"

	"gitlab.com/stefarf/go/learn-go/antlr4/steps/parser"
)

func (sc *stepsCompiler) EnterIfStmt(ctx *parser.IfStmtContext) {
	sc.pushElse()
	sc.pushLabel()
}

func (sc *stepsCompiler) ExitIfStmt(ctx *parser.IfStmtContext) {
	if !sc.getElse() {
		els := fmt.Sprintf("else%d", sc.getLabel())
		end := fmt.Sprintf("end%d", sc.getLabel())
		sc.setAddr(els)
		sc.setAddr(end)
		if DebugCompile {
			fmt.Printf("LABEL %s:\n", els)
			fmt.Printf("LABEL %s:\n", end)
		}
	}
	sc.popElse()
	sc.popLabel()
}

func (sc *stepsCompiler) EnterThenBlock(ctx *parser.ThenBlockContext) {
	sc.addPass2()

	label := fmt.Sprintf("else%d", sc.getLabel())
	sc.addMnemonic(Mnemonic{cmd: cmdPopAndJumpIfFalse, s: label})
	if DebugCompile {
		fmt.Printf("pop and jump to '%s' if false\n", label)
	}
}

func (sc *stepsCompiler) ExitThenBlock(ctx *parser.ThenBlockContext) {
	sc.addPass2()

	label := fmt.Sprintf("end%d", sc.getLabel())
	sc.addMnemonic(Mnemonic{cmd: cmdJump, s: label})
	if DebugCompile {
		fmt.Printf("jump to '%s'\n", label)
	}
}

func (sc *stepsCompiler) EnterElseBlock(ctx *parser.ElseBlockContext) {
	sc.setElse()
	label := fmt.Sprintf("else%d", sc.getLabel())
	sc.setAddr(label)
	if DebugCompile {
		fmt.Printf("LABEL %s:\n", label)
	}
}

func (sc *stepsCompiler) ExitElseBlock(ctx *parser.ElseBlockContext) {
	label := fmt.Sprintf("end%d", sc.getLabel())
	sc.setAddr(label)
	if DebugCompile {
		fmt.Printf("LABEL %s:\n", label)
	}
}
