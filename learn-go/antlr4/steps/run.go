package steps

import (
	"fmt"
	"log"
	"strconv"
)

type (
	Hook interface {
		Call(funcName string, param []*string) ([]*string, error)
		GetValue(id string) *string
		SetValue(id string, value *string)
	}
)

var (
	DebugRun = false
	opFunc   = map[string]func(run *stepsRunner, v1, v2 *string) *string{
		"and": func(run *stepsRunner, v1, v2 *string) *string {
			var v string
			if toBool(v1) && toBool(v2) {
				v = "1"
			} else {
				v = "0"
			}
			return &v
		},

		"or": func(run *stepsRunner, v1, v2 *string) *string {
			var v string
			if toBool(v1) || toBool(v2) {
				v = "1"
			} else {
				v = "0"
			}
			return &v
		},

		"==": func(run *stepsRunner, v1, v2 *string) *string {
			var v string
			if (v1 == nil && v2 == nil) || (v1 != nil && v2 != nil && *v1 == *v2) {
				v = "1"
			} else {
				v = "0"
			}
			return &v
		},

		"!=": func(run *stepsRunner, v1, v2 *string) *string {
			var v string
			if (v1 == nil && v2 == nil) || (v1 != nil && v2 != nil && *v1 == *v2) {
				v = "0"
			} else {
				v = "1"
			}
			return &v
		},

		"+": func(run *stepsRunner, v1, v2 *string) *string {
			s := strconv.FormatFloat(toNumber(v1)+toNumber(v2), 'f', -1, 64)
			return &s
		},

		"-": func(run *stepsRunner, v1, v2 *string) *string {
			s := strconv.FormatFloat(toNumber(v1)-toNumber(v2), 'f', -1, 64)
			return &s
		},

		"*": func(run *stepsRunner, v1, v2 *string) *string {
			s := strconv.FormatFloat(toNumber(v1)*toNumber(v2), 'f', -1, 64)
			return &s
		},

		"/": func(run *stepsRunner, v1, v2 *string) *string {
			s := strconv.FormatFloat(toNumber(v1)/toNumber(v2), 'f', -1, 64)
			return &s
		},
	}

	cmdExec = map[command]func(run *stepsRunner, pc int, mne Mnemonic) (int, error){
		cmdPopParamsCallAndSave: func(run *stepsRunner, pc int, mne Mnemonic) (i int, e error) {
			cmd := "pop params, call and save"
			params := mne.i
			funcName := mne.s
			ids := mne.a
			if DebugRun {
				log.Printf("pop %d params, call %s() and save result to %v\n",
					params, funcName, ids)
			}

			// Pop params and call
			rst, err := run.hook.Call(funcName, run.popParams(params))
			if err != nil {
				return 0, fmt.Errorf("%v, command: '%s'", err, cmd)
			}

			// Check and save return
			if len(ids) != 0 {
				if len(rst) != len(ids) {
					return 0, fmt.Errorf("error invalid number of returns, command: '%s'", cmd)
				}
				for idx, id := range ids {
					run.hook.SetValue(id, rst[idx])
				}
			}
			return pc + 1, nil
		},

		cmdPopParamsCallAndPush: func(run *stepsRunner, pc int, mne Mnemonic) (int, error) {
			cmd := "pop params, call and push"
			params := mne.i
			funcName := mne.s
			if DebugRun {
				log.Printf("pop %d params, call %s() and push result\n",
					params, funcName)
			}

			// Pop params and call
			rst, err := run.hook.Call(funcName, run.popParams(params))
			if err != nil {
				return 0, fmt.Errorf("%v, command: '%s'", err, cmd)
			}

			// Check return and push
			if len(rst) != 1 {
				return 0, fmt.Errorf("error must return 1 string, command: '%s'", cmd)
			}
			run.pushStack(rst[0])
			return pc + 1, nil
		},

		cmdPop2OperandAndPush: func(run *stepsRunner, pc int, mne Mnemonic) (int, error) {
			cmd := "pop 2, do operand and push result"
			op := mne.s
			if DebugRun {
				log.Printf("pop 2, do operand '%s' and push result\n", op)
			}

			v1 := run.popStack()
			v2 := run.popStack()

			f := opFunc[op]
			if f == nil {
				return 0, fmt.Errorf("error unknown op '%s', command: '%s'", op, cmd)
			}
			run.pushStack(f(run, v1, v2))
			return pc + 1, nil
		},

		cmdPushID: func(run *stepsRunner, pc int, mne Mnemonic) (int, error) {
			id := mne.s
			if DebugRun {
				log.Printf("push ID '%s'\n", id)
			}
			run.pushStack(run.hook.GetValue(id))
			return pc + 1, nil
		},

		cmdPushString: func(run *stepsRunner, pc int, mne Mnemonic) (int, error) {
			str := mne.s
			if DebugRun {
				log.Printf("push string '%s'\n", str)
			}
			run.pushStack(&str)
			return pc + 1, nil
		},

		cmdPushNumber: func(run *stepsRunner, pc int, mne Mnemonic) (i int, e error) {
			num := mne.s
			if DebugRun {
				log.Printf("push number '%s'\n", num)
			}
			run.pushStack(&num)
			return pc + 1, nil
		},

		cmdPopAndJumpIfFalse: func(run *stepsRunner, pc int, mne Mnemonic) (int, error) {
			label := mne.i
			if DebugRun {
				log.Printf("pop and jump to %d if false\n", label)
			}
			if toBool(run.popStack()) {
				// True
				return pc + 1, nil
			} else {
				// False
				return label, nil
			}
		},

		cmdJump: func(run *stepsRunner, pc int, mne Mnemonic) (int, error) {
			label := mne.i
			if DebugRun {
				log.Printf("jump to %d\n", label)
			}
			return label, nil
		},

		cmdPopAndAssign: func(run *stepsRunner, pc int, mne Mnemonic) (i int, e error) {
			id := mne.s
			if DebugRun {
				log.Printf("pop and assign '%s'\n", id)
			}
			run.hook.SetValue(id, run.popStack())
			return pc + 1, nil
		},

		cmdPopAndPushParam: func(run *stepsRunner, pc int, mne Mnemonic) (i int, e error) {
			if DebugRun {
				log.Println("pop and push param")
			}
			run.pushParam(run.popStack())
			return pc + 1, nil
		},
	}
)

func Run(compiled *Compiled, hook Hook) (err error) {
	if compiled == nil {
		return nil
	}

	run := stepsRunner{hook: hook}

	max := len(*compiled)
	pc := 0
	for {
		if pc >= max {
			return nil
		}
		mne := (*compiled)[pc]
		pc, err = cmdExec[mne.cmd](&run, pc, mne)
		if err != nil {
			return
		}
	}
}

func toBool(v *string) bool { return v != nil && *v == "1" }

func toNumber(v *string) float64 {
	if v == nil {
		return 0
	}
	f, err := strconv.ParseFloat(*v, 64)
	if err != nil {
		return 0
	}
	return f
}
