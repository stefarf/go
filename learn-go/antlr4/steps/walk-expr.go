package steps

import (
	"fmt"

	"gitlab.com/stefarf/go/learn-go/antlr4/steps/parser"
)

func (sc *stepsCompiler) ExitIDExpr(ctx *parser.IDExprContext) {
	id := ctx.GetText()
	sc.addMnemonic(Mnemonic{cmd: cmdPushID, s: id})
	if DebugCompile {
		fmt.Printf("push ID '%s'\n", id)
	}
}

func (sc *stepsCompiler) ExitNumExpr(ctx *parser.NumExprContext) {
	num := ctx.GetText()
	sc.addMnemonic(Mnemonic{cmd: cmdPushNumber, s: num})
	if DebugCompile {
		fmt.Printf("push number '%s'\n", num)
	}
}

func (sc *stepsCompiler) ExitStrExpr(ctx *parser.StrExprContext) {
	str := toString(ctx.GetText())
	sc.addMnemonic(Mnemonic{cmd: cmdPushString, s: str})
	if DebugCompile {
		fmt.Printf("push string '%s'\n", str)
	}
}

func (sc *stepsCompiler) ExitCmpExpr(ctx *parser.CmpExprContext) {
	op := ctx.OpCmp().GetText()
	sc.addMnemonic(Mnemonic{cmd: cmdPop2OperandAndPush, s: op})
	if DebugCompile {
		fmt.Printf("pop 2, do operand '%s' and push result\n", op)
	}
}

func (sc *stepsCompiler) ExitAndOrExpr(ctx *parser.AndOrExprContext) {
	op := ctx.OpAndOr().GetText()
	sc.addMnemonic(Mnemonic{cmd: cmdPop2OperandAndPush, s: op})
	if DebugCompile {
		fmt.Printf("pop 2, do operand '%s' and push result\n", op)
	}
}

func (sc *stepsCompiler) ExitMathExpr(ctx *parser.MathExprContext) {
	op := ctx.OpMath().GetText()
	sc.addMnemonic(Mnemonic{cmd: cmdPop2OperandAndPush, s: op})
	if DebugCompile {
		fmt.Printf("pop 2, do operand '%s' and push result\n", op)
	}
}
