// Code generated from Steps.g4 by ANTLR 4.7.1. DO NOT EDIT.

package parser

import (
	"fmt"
	"unicode"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = unicode.IsLetter

var serializedLexerAtn = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 2, 23, 116,
	8, 1, 4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7,
	9, 7, 4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12,
	4, 13, 9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4,
	18, 9, 18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 3, 2,
	3, 2, 3, 3, 3, 3, 3, 4, 3, 4, 3, 5, 3, 5, 3, 6, 3, 6, 3, 6, 3, 7, 3, 7,
	3, 7, 3, 7, 3, 7, 3, 8, 3, 8, 3, 9, 3, 9, 3, 10, 3, 10, 3, 11, 3, 11, 3,
	12, 3, 12, 3, 13, 3, 13, 3, 14, 3, 14, 3, 15, 3, 15, 3, 15, 3, 16, 3, 16,
	3, 16, 3, 17, 3, 17, 3, 17, 3, 17, 3, 18, 3, 18, 3, 18, 3, 19, 3, 19, 7,
	19, 91, 10, 19, 12, 19, 14, 19, 94, 11, 19, 3, 20, 3, 20, 7, 20, 98, 10,
	20, 12, 20, 14, 20, 101, 11, 20, 3, 20, 3, 20, 3, 21, 6, 21, 106, 10, 21,
	13, 21, 14, 21, 107, 3, 22, 6, 22, 111, 10, 22, 13, 22, 14, 22, 112, 3,
	22, 3, 22, 2, 2, 23, 3, 3, 5, 4, 7, 5, 9, 6, 11, 7, 13, 8, 15, 9, 17, 10,
	19, 11, 21, 12, 23, 13, 25, 14, 27, 15, 29, 16, 31, 17, 33, 18, 35, 19,
	37, 20, 39, 21, 41, 22, 43, 23, 3, 2, 7, 6, 2, 38, 38, 67, 92, 97, 97,
	99, 124, 7, 2, 38, 38, 50, 59, 67, 92, 97, 97, 99, 124, 5, 2, 12, 12, 15,
	15, 41, 41, 4, 2, 48, 48, 50, 59, 5, 2, 11, 12, 15, 15, 34, 34, 2, 119,
	2, 3, 3, 2, 2, 2, 2, 5, 3, 2, 2, 2, 2, 7, 3, 2, 2, 2, 2, 9, 3, 2, 2, 2,
	2, 11, 3, 2, 2, 2, 2, 13, 3, 2, 2, 2, 2, 15, 3, 2, 2, 2, 2, 17, 3, 2, 2,
	2, 2, 19, 3, 2, 2, 2, 2, 21, 3, 2, 2, 2, 2, 23, 3, 2, 2, 2, 2, 25, 3, 2,
	2, 2, 2, 27, 3, 2, 2, 2, 2, 29, 3, 2, 2, 2, 2, 31, 3, 2, 2, 2, 2, 33, 3,
	2, 2, 2, 2, 35, 3, 2, 2, 2, 2, 37, 3, 2, 2, 2, 2, 39, 3, 2, 2, 2, 2, 41,
	3, 2, 2, 2, 2, 43, 3, 2, 2, 2, 3, 45, 3, 2, 2, 2, 5, 47, 3, 2, 2, 2, 7,
	49, 3, 2, 2, 2, 9, 51, 3, 2, 2, 2, 11, 53, 3, 2, 2, 2, 13, 56, 3, 2, 2,
	2, 15, 61, 3, 2, 2, 2, 17, 63, 3, 2, 2, 2, 19, 65, 3, 2, 2, 2, 21, 67,
	3, 2, 2, 2, 23, 69, 3, 2, 2, 2, 25, 71, 3, 2, 2, 2, 27, 73, 3, 2, 2, 2,
	29, 75, 3, 2, 2, 2, 31, 78, 3, 2, 2, 2, 33, 81, 3, 2, 2, 2, 35, 85, 3,
	2, 2, 2, 37, 88, 3, 2, 2, 2, 39, 95, 3, 2, 2, 2, 41, 105, 3, 2, 2, 2, 43,
	110, 3, 2, 2, 2, 45, 46, 7, 63, 2, 2, 46, 4, 3, 2, 2, 2, 47, 48, 7, 42,
	2, 2, 48, 6, 3, 2, 2, 2, 49, 50, 7, 43, 2, 2, 50, 8, 3, 2, 2, 2, 51, 52,
	7, 61, 2, 2, 52, 10, 3, 2, 2, 2, 53, 54, 7, 107, 2, 2, 54, 55, 7, 104,
	2, 2, 55, 12, 3, 2, 2, 2, 56, 57, 7, 103, 2, 2, 57, 58, 7, 110, 2, 2, 58,
	59, 7, 117, 2, 2, 59, 60, 7, 103, 2, 2, 60, 14, 3, 2, 2, 2, 61, 62, 7,
	46, 2, 2, 62, 16, 3, 2, 2, 2, 63, 64, 7, 125, 2, 2, 64, 18, 3, 2, 2, 2,
	65, 66, 7, 127, 2, 2, 66, 20, 3, 2, 2, 2, 67, 68, 7, 45, 2, 2, 68, 22,
	3, 2, 2, 2, 69, 70, 7, 47, 2, 2, 70, 24, 3, 2, 2, 2, 71, 72, 7, 44, 2,
	2, 72, 26, 3, 2, 2, 2, 73, 74, 7, 49, 2, 2, 74, 28, 3, 2, 2, 2, 75, 76,
	7, 63, 2, 2, 76, 77, 7, 63, 2, 2, 77, 30, 3, 2, 2, 2, 78, 79, 7, 35, 2,
	2, 79, 80, 7, 63, 2, 2, 80, 32, 3, 2, 2, 2, 81, 82, 7, 99, 2, 2, 82, 83,
	7, 112, 2, 2, 83, 84, 7, 102, 2, 2, 84, 34, 3, 2, 2, 2, 85, 86, 7, 113,
	2, 2, 86, 87, 7, 116, 2, 2, 87, 36, 3, 2, 2, 2, 88, 92, 9, 2, 2, 2, 89,
	91, 9, 3, 2, 2, 90, 89, 3, 2, 2, 2, 91, 94, 3, 2, 2, 2, 92, 90, 3, 2, 2,
	2, 92, 93, 3, 2, 2, 2, 93, 38, 3, 2, 2, 2, 94, 92, 3, 2, 2, 2, 95, 99,
	7, 41, 2, 2, 96, 98, 10, 4, 2, 2, 97, 96, 3, 2, 2, 2, 98, 101, 3, 2, 2,
	2, 99, 97, 3, 2, 2, 2, 99, 100, 3, 2, 2, 2, 100, 102, 3, 2, 2, 2, 101,
	99, 3, 2, 2, 2, 102, 103, 7, 41, 2, 2, 103, 40, 3, 2, 2, 2, 104, 106, 9,
	5, 2, 2, 105, 104, 3, 2, 2, 2, 106, 107, 3, 2, 2, 2, 107, 105, 3, 2, 2,
	2, 107, 108, 3, 2, 2, 2, 108, 42, 3, 2, 2, 2, 109, 111, 9, 6, 2, 2, 110,
	109, 3, 2, 2, 2, 111, 112, 3, 2, 2, 2, 112, 110, 3, 2, 2, 2, 112, 113,
	3, 2, 2, 2, 113, 114, 3, 2, 2, 2, 114, 115, 8, 22, 2, 2, 115, 44, 3, 2,
	2, 2, 7, 2, 92, 99, 107, 112, 3, 8, 2, 2,
}

var lexerDeserializer = antlr.NewATNDeserializer(nil)
var lexerAtn = lexerDeserializer.DeserializeFromUInt16(serializedLexerAtn)

var lexerChannelNames = []string{
	"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
}

var lexerModeNames = []string{
	"DEFAULT_MODE",
}

var lexerLiteralNames = []string{
	"", "'='", "'('", "')'", "';'", "'if'", "'else'", "','", "'{'", "'}'",
	"'+'", "'-'", "'*'", "'/'", "'=='", "'!='", "'and'", "'or'",
}

var lexerSymbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"ID", "STRING", "NUMBER", "WHITESPACE",
}

var lexerRuleNames = []string{
	"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8",
	"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16",
	"ID", "STRING", "NUMBER", "WHITESPACE",
}

type StepsLexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var lexerDecisionToDFA = make([]*antlr.DFA, len(lexerAtn.DecisionToState))

func init() {
	for index, ds := range lexerAtn.DecisionToState {
		lexerDecisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

func NewStepsLexer(input antlr.CharStream) *StepsLexer {

	l := new(StepsLexer)

	l.BaseLexer = antlr.NewBaseLexer(input)
	l.Interpreter = antlr.NewLexerATNSimulator(l, lexerAtn, lexerDecisionToDFA, antlr.NewPredictionContextCache())

	l.channelNames = lexerChannelNames
	l.modeNames = lexerModeNames
	l.RuleNames = lexerRuleNames
	l.LiteralNames = lexerLiteralNames
	l.SymbolicNames = lexerSymbolicNames
	l.GrammarFileName = "Steps.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// StepsLexer tokens.
const (
	StepsLexerT__0       = 1
	StepsLexerT__1       = 2
	StepsLexerT__2       = 3
	StepsLexerT__3       = 4
	StepsLexerT__4       = 5
	StepsLexerT__5       = 6
	StepsLexerT__6       = 7
	StepsLexerT__7       = 8
	StepsLexerT__8       = 9
	StepsLexerT__9       = 10
	StepsLexerT__10      = 11
	StepsLexerT__11      = 12
	StepsLexerT__12      = 13
	StepsLexerT__13      = 14
	StepsLexerT__14      = 15
	StepsLexerT__15      = 16
	StepsLexerT__16      = 17
	StepsLexerID         = 18
	StepsLexerSTRING     = 19
	StepsLexerNUMBER     = 20
	StepsLexerWHITESPACE = 21
)
