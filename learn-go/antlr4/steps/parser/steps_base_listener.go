// Code generated from Steps.g4 by ANTLR 4.7.1. DO NOT EDIT.

package parser // Steps

import "github.com/antlr/antlr4/runtime/Go/antlr"

// BaseStepsListener is a complete listener for a parse tree produced by StepsParser.
type BaseStepsListener struct{}

var _ StepsListener = &BaseStepsListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseStepsListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseStepsListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseStepsListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseStepsListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterStart is called when production start is entered.
func (s *BaseStepsListener) EnterStart(ctx *StartContext) {}

// ExitStart is called when production start is exited.
func (s *BaseStepsListener) ExitStart(ctx *StartContext) {}

// EnterFuncStmt is called when production FuncStmt is entered.
func (s *BaseStepsListener) EnterFuncStmt(ctx *FuncStmtContext) {}

// ExitFuncStmt is called when production FuncStmt is exited.
func (s *BaseStepsListener) ExitFuncStmt(ctx *FuncStmtContext) {}

// EnterIfStmt is called when production IfStmt is entered.
func (s *BaseStepsListener) EnterIfStmt(ctx *IfStmtContext) {}

// ExitIfStmt is called when production IfStmt is exited.
func (s *BaseStepsListener) ExitIfStmt(ctx *IfStmtContext) {}

// EnterAssignment is called when production Assignment is entered.
func (s *BaseStepsListener) EnterAssignment(ctx *AssignmentContext) {}

// ExitAssignment is called when production Assignment is exited.
func (s *BaseStepsListener) ExitAssignment(ctx *AssignmentContext) {}

// EnterFuncReturns is called when production funcReturns is entered.
func (s *BaseStepsListener) EnterFuncReturns(ctx *FuncReturnsContext) {}

// ExitFuncReturns is called when production funcReturns is exited.
func (s *BaseStepsListener) ExitFuncReturns(ctx *FuncReturnsContext) {}

// EnterFuncParams is called when production funcParams is entered.
func (s *BaseStepsListener) EnterFuncParams(ctx *FuncParamsContext) {}

// ExitFuncParams is called when production funcParams is exited.
func (s *BaseStepsListener) ExitFuncParams(ctx *FuncParamsContext) {}

// EnterFuncParam is called when production funcParam is entered.
func (s *BaseStepsListener) EnterFuncParam(ctx *FuncParamContext) {}

// ExitFuncParam is called when production funcParam is exited.
func (s *BaseStepsListener) ExitFuncParam(ctx *FuncParamContext) {}

// EnterThenBlock is called when production thenBlock is entered.
func (s *BaseStepsListener) EnterThenBlock(ctx *ThenBlockContext) {}

// ExitThenBlock is called when production thenBlock is exited.
func (s *BaseStepsListener) ExitThenBlock(ctx *ThenBlockContext) {}

// EnterElseBlock is called when production elseBlock is entered.
func (s *BaseStepsListener) EnterElseBlock(ctx *ElseBlockContext) {}

// ExitElseBlock is called when production elseBlock is exited.
func (s *BaseStepsListener) ExitElseBlock(ctx *ElseBlockContext) {}

// EnterBlock is called when production block is entered.
func (s *BaseStepsListener) EnterBlock(ctx *BlockContext) {}

// ExitBlock is called when production block is exited.
func (s *BaseStepsListener) ExitBlock(ctx *BlockContext) {}

// EnterIDExpr is called when production IDExpr is entered.
func (s *BaseStepsListener) EnterIDExpr(ctx *IDExprContext) {}

// ExitIDExpr is called when production IDExpr is exited.
func (s *BaseStepsListener) ExitIDExpr(ctx *IDExprContext) {}

// EnterNumExpr is called when production NumExpr is entered.
func (s *BaseStepsListener) EnterNumExpr(ctx *NumExprContext) {}

// ExitNumExpr is called when production NumExpr is exited.
func (s *BaseStepsListener) ExitNumExpr(ctx *NumExprContext) {}

// EnterParExpr is called when production ParExpr is entered.
func (s *BaseStepsListener) EnterParExpr(ctx *ParExprContext) {}

// ExitParExpr is called when production ParExpr is exited.
func (s *BaseStepsListener) ExitParExpr(ctx *ParExprContext) {}

// EnterCmpExpr is called when production CmpExpr is entered.
func (s *BaseStepsListener) EnterCmpExpr(ctx *CmpExprContext) {}

// ExitCmpExpr is called when production CmpExpr is exited.
func (s *BaseStepsListener) ExitCmpExpr(ctx *CmpExprContext) {}

// EnterAndOrExpr is called when production AndOrExpr is entered.
func (s *BaseStepsListener) EnterAndOrExpr(ctx *AndOrExprContext) {}

// ExitAndOrExpr is called when production AndOrExpr is exited.
func (s *BaseStepsListener) ExitAndOrExpr(ctx *AndOrExprContext) {}

// EnterStrExpr is called when production StrExpr is entered.
func (s *BaseStepsListener) EnterStrExpr(ctx *StrExprContext) {}

// ExitStrExpr is called when production StrExpr is exited.
func (s *BaseStepsListener) ExitStrExpr(ctx *StrExprContext) {}

// EnterFuncExpr is called when production FuncExpr is entered.
func (s *BaseStepsListener) EnterFuncExpr(ctx *FuncExprContext) {}

// ExitFuncExpr is called when production FuncExpr is exited.
func (s *BaseStepsListener) ExitFuncExpr(ctx *FuncExprContext) {}

// EnterMathExpr is called when production MathExpr is entered.
func (s *BaseStepsListener) EnterMathExpr(ctx *MathExprContext) {}

// ExitMathExpr is called when production MathExpr is exited.
func (s *BaseStepsListener) ExitMathExpr(ctx *MathExprContext) {}

// EnterOpMath is called when production opMath is entered.
func (s *BaseStepsListener) EnterOpMath(ctx *OpMathContext) {}

// ExitOpMath is called when production opMath is exited.
func (s *BaseStepsListener) ExitOpMath(ctx *OpMathContext) {}

// EnterOpCmp is called when production opCmp is entered.
func (s *BaseStepsListener) EnterOpCmp(ctx *OpCmpContext) {}

// ExitOpCmp is called when production opCmp is exited.
func (s *BaseStepsListener) ExitOpCmp(ctx *OpCmpContext) {}

// EnterOpAndOr is called when production opAndOr is entered.
func (s *BaseStepsListener) EnterOpAndOr(ctx *OpAndOrContext) {}

// ExitOpAndOr is called when production opAndOr is exited.
func (s *BaseStepsListener) ExitOpAndOr(ctx *OpAndOrContext) {}
