// Code generated from Steps.g4 by ANTLR 4.7.1. DO NOT EDIT.

package parser // Steps

import "github.com/antlr/antlr4/runtime/Go/antlr"

// StepsListener is a complete listener for a parse tree produced by StepsParser.
type StepsListener interface {
	antlr.ParseTreeListener

	// EnterStart is called when entering the start production.
	EnterStart(c *StartContext)

	// EnterFuncStmt is called when entering the FuncStmt production.
	EnterFuncStmt(c *FuncStmtContext)

	// EnterIfStmt is called when entering the IfStmt production.
	EnterIfStmt(c *IfStmtContext)

	// EnterAssignment is called when entering the Assignment production.
	EnterAssignment(c *AssignmentContext)

	// EnterFuncReturns is called when entering the funcReturns production.
	EnterFuncReturns(c *FuncReturnsContext)

	// EnterFuncParams is called when entering the funcParams production.
	EnterFuncParams(c *FuncParamsContext)

	// EnterFuncParam is called when entering the funcParam production.
	EnterFuncParam(c *FuncParamContext)

	// EnterThenBlock is called when entering the thenBlock production.
	EnterThenBlock(c *ThenBlockContext)

	// EnterElseBlock is called when entering the elseBlock production.
	EnterElseBlock(c *ElseBlockContext)

	// EnterBlock is called when entering the block production.
	EnterBlock(c *BlockContext)

	// EnterIDExpr is called when entering the IDExpr production.
	EnterIDExpr(c *IDExprContext)

	// EnterNumExpr is called when entering the NumExpr production.
	EnterNumExpr(c *NumExprContext)

	// EnterParExpr is called when entering the ParExpr production.
	EnterParExpr(c *ParExprContext)

	// EnterCmpExpr is called when entering the CmpExpr production.
	EnterCmpExpr(c *CmpExprContext)

	// EnterAndOrExpr is called when entering the AndOrExpr production.
	EnterAndOrExpr(c *AndOrExprContext)

	// EnterStrExpr is called when entering the StrExpr production.
	EnterStrExpr(c *StrExprContext)

	// EnterFuncExpr is called when entering the FuncExpr production.
	EnterFuncExpr(c *FuncExprContext)

	// EnterMathExpr is called when entering the MathExpr production.
	EnterMathExpr(c *MathExprContext)

	// EnterOpMath is called when entering the opMath production.
	EnterOpMath(c *OpMathContext)

	// EnterOpCmp is called when entering the opCmp production.
	EnterOpCmp(c *OpCmpContext)

	// EnterOpAndOr is called when entering the opAndOr production.
	EnterOpAndOr(c *OpAndOrContext)

	// ExitStart is called when exiting the start production.
	ExitStart(c *StartContext)

	// ExitFuncStmt is called when exiting the FuncStmt production.
	ExitFuncStmt(c *FuncStmtContext)

	// ExitIfStmt is called when exiting the IfStmt production.
	ExitIfStmt(c *IfStmtContext)

	// ExitAssignment is called when exiting the Assignment production.
	ExitAssignment(c *AssignmentContext)

	// ExitFuncReturns is called when exiting the funcReturns production.
	ExitFuncReturns(c *FuncReturnsContext)

	// ExitFuncParams is called when exiting the funcParams production.
	ExitFuncParams(c *FuncParamsContext)

	// ExitFuncParam is called when exiting the funcParam production.
	ExitFuncParam(c *FuncParamContext)

	// ExitThenBlock is called when exiting the thenBlock production.
	ExitThenBlock(c *ThenBlockContext)

	// ExitElseBlock is called when exiting the elseBlock production.
	ExitElseBlock(c *ElseBlockContext)

	// ExitBlock is called when exiting the block production.
	ExitBlock(c *BlockContext)

	// ExitIDExpr is called when exiting the IDExpr production.
	ExitIDExpr(c *IDExprContext)

	// ExitNumExpr is called when exiting the NumExpr production.
	ExitNumExpr(c *NumExprContext)

	// ExitParExpr is called when exiting the ParExpr production.
	ExitParExpr(c *ParExprContext)

	// ExitCmpExpr is called when exiting the CmpExpr production.
	ExitCmpExpr(c *CmpExprContext)

	// ExitAndOrExpr is called when exiting the AndOrExpr production.
	ExitAndOrExpr(c *AndOrExprContext)

	// ExitStrExpr is called when exiting the StrExpr production.
	ExitStrExpr(c *StrExprContext)

	// ExitFuncExpr is called when exiting the FuncExpr production.
	ExitFuncExpr(c *FuncExprContext)

	// ExitMathExpr is called when exiting the MathExpr production.
	ExitMathExpr(c *MathExprContext)

	// ExitOpMath is called when exiting the opMath production.
	ExitOpMath(c *OpMathContext)

	// ExitOpCmp is called when exiting the opCmp production.
	ExitOpCmp(c *OpCmpContext)

	// ExitOpAndOr is called when exiting the opAndOr production.
	ExitOpAndOr(c *OpAndOrContext)
}
