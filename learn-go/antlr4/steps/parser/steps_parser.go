// Code generated from Steps.g4 by ANTLR 4.7.1. DO NOT EDIT.

package parser // Steps

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 23, 133,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 3, 2, 7, 2, 28, 10, 2, 12, 2, 14, 2, 31, 11, 2, 3, 2, 3, 2, 3, 3,
	3, 3, 3, 3, 5, 3, 38, 10, 3, 3, 3, 3, 3, 3, 3, 5, 3, 43, 10, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 3, 52, 10, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 5, 3, 59, 10, 3, 3, 4, 3, 4, 3, 4, 7, 4, 64, 10, 4, 12, 4, 14,
	4, 67, 11, 4, 3, 5, 3, 5, 3, 5, 7, 5, 72, 10, 5, 12, 5, 14, 5, 75, 11,
	5, 3, 6, 3, 6, 3, 7, 3, 7, 3, 8, 3, 8, 3, 9, 3, 9, 7, 9, 85, 10, 9, 12,
	9, 14, 9, 88, 11, 9, 3, 9, 3, 9, 5, 9, 92, 10, 9, 3, 10, 3, 10, 3, 10,
	3, 10, 5, 10, 98, 10, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3,
	10, 3, 10, 5, 10, 108, 10, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10,
	3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 7, 10, 122, 10, 10, 12, 10, 14,
	10, 125, 11, 10, 3, 11, 3, 11, 3, 12, 3, 12, 3, 13, 3, 13, 3, 13, 2, 3,
	18, 14, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 2, 5, 3, 2, 12, 15,
	3, 2, 16, 17, 3, 2, 18, 19, 2, 138, 2, 29, 3, 2, 2, 2, 4, 58, 3, 2, 2,
	2, 6, 60, 3, 2, 2, 2, 8, 68, 3, 2, 2, 2, 10, 76, 3, 2, 2, 2, 12, 78, 3,
	2, 2, 2, 14, 80, 3, 2, 2, 2, 16, 91, 3, 2, 2, 2, 18, 107, 3, 2, 2, 2, 20,
	126, 3, 2, 2, 2, 22, 128, 3, 2, 2, 2, 24, 130, 3, 2, 2, 2, 26, 28, 5, 4,
	3, 2, 27, 26, 3, 2, 2, 2, 28, 31, 3, 2, 2, 2, 29, 27, 3, 2, 2, 2, 29, 30,
	3, 2, 2, 2, 30, 32, 3, 2, 2, 2, 31, 29, 3, 2, 2, 2, 32, 33, 7, 2, 2, 3,
	33, 3, 3, 2, 2, 2, 34, 35, 5, 6, 4, 2, 35, 36, 7, 3, 2, 2, 36, 38, 3, 2,
	2, 2, 37, 34, 3, 2, 2, 2, 37, 38, 3, 2, 2, 2, 38, 39, 3, 2, 2, 2, 39, 40,
	7, 20, 2, 2, 40, 42, 7, 4, 2, 2, 41, 43, 5, 8, 5, 2, 42, 41, 3, 2, 2, 2,
	42, 43, 3, 2, 2, 2, 43, 44, 3, 2, 2, 2, 44, 45, 7, 5, 2, 2, 45, 59, 7,
	6, 2, 2, 46, 47, 7, 7, 2, 2, 47, 48, 5, 18, 10, 2, 48, 51, 5, 12, 7, 2,
	49, 50, 7, 8, 2, 2, 50, 52, 5, 14, 8, 2, 51, 49, 3, 2, 2, 2, 51, 52, 3,
	2, 2, 2, 52, 59, 3, 2, 2, 2, 53, 54, 7, 20, 2, 2, 54, 55, 7, 3, 2, 2, 55,
	56, 5, 18, 10, 2, 56, 57, 7, 6, 2, 2, 57, 59, 3, 2, 2, 2, 58, 37, 3, 2,
	2, 2, 58, 46, 3, 2, 2, 2, 58, 53, 3, 2, 2, 2, 59, 5, 3, 2, 2, 2, 60, 65,
	7, 20, 2, 2, 61, 62, 7, 9, 2, 2, 62, 64, 7, 20, 2, 2, 63, 61, 3, 2, 2,
	2, 64, 67, 3, 2, 2, 2, 65, 63, 3, 2, 2, 2, 65, 66, 3, 2, 2, 2, 66, 7, 3,
	2, 2, 2, 67, 65, 3, 2, 2, 2, 68, 73, 5, 10, 6, 2, 69, 70, 7, 9, 2, 2, 70,
	72, 5, 10, 6, 2, 71, 69, 3, 2, 2, 2, 72, 75, 3, 2, 2, 2, 73, 71, 3, 2,
	2, 2, 73, 74, 3, 2, 2, 2, 74, 9, 3, 2, 2, 2, 75, 73, 3, 2, 2, 2, 76, 77,
	5, 18, 10, 2, 77, 11, 3, 2, 2, 2, 78, 79, 5, 16, 9, 2, 79, 13, 3, 2, 2,
	2, 80, 81, 5, 16, 9, 2, 81, 15, 3, 2, 2, 2, 82, 86, 7, 10, 2, 2, 83, 85,
	5, 4, 3, 2, 84, 83, 3, 2, 2, 2, 85, 88, 3, 2, 2, 2, 86, 84, 3, 2, 2, 2,
	86, 87, 3, 2, 2, 2, 87, 89, 3, 2, 2, 2, 88, 86, 3, 2, 2, 2, 89, 92, 7,
	11, 2, 2, 90, 92, 5, 4, 3, 2, 91, 82, 3, 2, 2, 2, 91, 90, 3, 2, 2, 2, 92,
	17, 3, 2, 2, 2, 93, 94, 8, 10, 1, 2, 94, 95, 7, 20, 2, 2, 95, 97, 7, 4,
	2, 2, 96, 98, 5, 8, 5, 2, 97, 96, 3, 2, 2, 2, 97, 98, 3, 2, 2, 2, 98, 99,
	3, 2, 2, 2, 99, 108, 7, 5, 2, 2, 100, 108, 7, 20, 2, 2, 101, 108, 7, 21,
	2, 2, 102, 108, 7, 22, 2, 2, 103, 104, 7, 4, 2, 2, 104, 105, 5, 18, 10,
	2, 105, 106, 7, 5, 2, 2, 106, 108, 3, 2, 2, 2, 107, 93, 3, 2, 2, 2, 107,
	100, 3, 2, 2, 2, 107, 101, 3, 2, 2, 2, 107, 102, 3, 2, 2, 2, 107, 103,
	3, 2, 2, 2, 108, 123, 3, 2, 2, 2, 109, 110, 12, 5, 2, 2, 110, 111, 5, 20,
	11, 2, 111, 112, 5, 18, 10, 6, 112, 122, 3, 2, 2, 2, 113, 114, 12, 4, 2,
	2, 114, 115, 5, 22, 12, 2, 115, 116, 5, 18, 10, 5, 116, 122, 3, 2, 2, 2,
	117, 118, 12, 3, 2, 2, 118, 119, 5, 24, 13, 2, 119, 120, 5, 18, 10, 4,
	120, 122, 3, 2, 2, 2, 121, 109, 3, 2, 2, 2, 121, 113, 3, 2, 2, 2, 121,
	117, 3, 2, 2, 2, 122, 125, 3, 2, 2, 2, 123, 121, 3, 2, 2, 2, 123, 124,
	3, 2, 2, 2, 124, 19, 3, 2, 2, 2, 125, 123, 3, 2, 2, 2, 126, 127, 9, 2,
	2, 2, 127, 21, 3, 2, 2, 2, 128, 129, 9, 3, 2, 2, 129, 23, 3, 2, 2, 2, 130,
	131, 9, 4, 2, 2, 131, 25, 3, 2, 2, 2, 15, 29, 37, 42, 51, 58, 65, 73, 86,
	91, 97, 107, 121, 123,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'='", "'('", "')'", "';'", "'if'", "'else'", "','", "'{'", "'}'",
	"'+'", "'-'", "'*'", "'/'", "'=='", "'!='", "'and'", "'or'",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"ID", "STRING", "NUMBER", "WHITESPACE",
}

var ruleNames = []string{
	"start", "statement", "funcReturns", "funcParams", "funcParam", "thenBlock",
	"elseBlock", "block", "expression", "opMath", "opCmp", "opAndOr",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type StepsParser struct {
	*antlr.BaseParser
}

func NewStepsParser(input antlr.TokenStream) *StepsParser {
	this := new(StepsParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "Steps.g4"

	return this
}

// StepsParser tokens.
const (
	StepsParserEOF        = antlr.TokenEOF
	StepsParserT__0       = 1
	StepsParserT__1       = 2
	StepsParserT__2       = 3
	StepsParserT__3       = 4
	StepsParserT__4       = 5
	StepsParserT__5       = 6
	StepsParserT__6       = 7
	StepsParserT__7       = 8
	StepsParserT__8       = 9
	StepsParserT__9       = 10
	StepsParserT__10      = 11
	StepsParserT__11      = 12
	StepsParserT__12      = 13
	StepsParserT__13      = 14
	StepsParserT__14      = 15
	StepsParserT__15      = 16
	StepsParserT__16      = 17
	StepsParserID         = 18
	StepsParserSTRING     = 19
	StepsParserNUMBER     = 20
	StepsParserWHITESPACE = 21
)

// StepsParser rules.
const (
	StepsParserRULE_start       = 0
	StepsParserRULE_statement   = 1
	StepsParserRULE_funcReturns = 2
	StepsParserRULE_funcParams  = 3
	StepsParserRULE_funcParam   = 4
	StepsParserRULE_thenBlock   = 5
	StepsParserRULE_elseBlock   = 6
	StepsParserRULE_block       = 7
	StepsParserRULE_expression  = 8
	StepsParserRULE_opMath      = 9
	StepsParserRULE_opCmp       = 10
	StepsParserRULE_opAndOr     = 11
)

// IStartContext is an interface to support dynamic dispatch.
type IStartContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartContext differentiates from other interfaces.
	IsStartContext()
}

type StartContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartContext() *StartContext {
	var p = new(StartContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_start
	return p
}

func (*StartContext) IsStartContext() {}

func NewStartContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartContext {
	var p = new(StartContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_start

	return p
}

func (s *StartContext) GetParser() antlr.Parser { return s.parser }

func (s *StartContext) EOF() antlr.TerminalNode {
	return s.GetToken(StepsParserEOF, 0)
}

func (s *StartContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *StartContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *StartContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterStart(s)
	}
}

func (s *StartContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitStart(s)
	}
}

func (p *StepsParser) Start() (localctx IStartContext) {
	localctx = NewStartContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, StepsParserRULE_start)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(27)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == StepsParserT__4 || _la == StepsParserID {
		{
			p.SetState(24)
			p.Statement()
		}

		p.SetState(29)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(30)
		p.Match(StepsParserEOF)
	}

	return localctx
}

// IStatementContext is an interface to support dynamic dispatch.
type IStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStatementContext differentiates from other interfaces.
	IsStatementContext()
}

type StatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStatementContext() *StatementContext {
	var p = new(StatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_statement
	return p
}

func (*StatementContext) IsStatementContext() {}

func NewStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StatementContext {
	var p = new(StatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_statement

	return p
}

func (s *StatementContext) GetParser() antlr.Parser { return s.parser }

func (s *StatementContext) CopyFrom(ctx *StatementContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *StatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type AssignmentContext struct {
	*StatementContext
}

func NewAssignmentContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AssignmentContext {
	var p = new(AssignmentContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *AssignmentContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AssignmentContext) ID() antlr.TerminalNode {
	return s.GetToken(StepsParserID, 0)
}

func (s *AssignmentContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *AssignmentContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterAssignment(s)
	}
}

func (s *AssignmentContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitAssignment(s)
	}
}

type IfStmtContext struct {
	*StatementContext
}

func NewIfStmtContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IfStmtContext {
	var p = new(IfStmtContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *IfStmtContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfStmtContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IfStmtContext) ThenBlock() IThenBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IThenBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IThenBlockContext)
}

func (s *IfStmtContext) ElseBlock() IElseBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IElseBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IElseBlockContext)
}

func (s *IfStmtContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterIfStmt(s)
	}
}

func (s *IfStmtContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitIfStmt(s)
	}
}

type FuncStmtContext struct {
	*StatementContext
}

func NewFuncStmtContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *FuncStmtContext {
	var p = new(FuncStmtContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *FuncStmtContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FuncStmtContext) ID() antlr.TerminalNode {
	return s.GetToken(StepsParserID, 0)
}

func (s *FuncStmtContext) FuncReturns() IFuncReturnsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFuncReturnsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFuncReturnsContext)
}

func (s *FuncStmtContext) FuncParams() IFuncParamsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFuncParamsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFuncParamsContext)
}

func (s *FuncStmtContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterFuncStmt(s)
	}
}

func (s *FuncStmtContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitFuncStmt(s)
	}
}

func (p *StepsParser) Statement() (localctx IStatementContext) {
	localctx = NewStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, StepsParserRULE_statement)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(56)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 4, p.GetParserRuleContext()) {
	case 1:
		localctx = NewFuncStmtContext(p, localctx)
		p.EnterOuterAlt(localctx, 1)
		p.SetState(35)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 1, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(32)
				p.FuncReturns()
			}
			{
				p.SetState(33)
				p.Match(StepsParserT__0)
			}

		}
		{
			p.SetState(37)
			p.Match(StepsParserID)
		}
		{
			p.SetState(38)
			p.Match(StepsParserT__1)
		}
		p.SetState(40)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<StepsParserT__1)|(1<<StepsParserID)|(1<<StepsParserSTRING)|(1<<StepsParserNUMBER))) != 0 {
			{
				p.SetState(39)
				p.FuncParams()
			}

		}
		{
			p.SetState(42)
			p.Match(StepsParserT__2)
		}
		{
			p.SetState(43)
			p.Match(StepsParserT__3)
		}

	case 2:
		localctx = NewIfStmtContext(p, localctx)
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(44)
			p.Match(StepsParserT__4)
		}
		{
			p.SetState(45)
			p.expression(0)
		}
		{
			p.SetState(46)
			p.ThenBlock()
		}
		p.SetState(49)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 3, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(47)
				p.Match(StepsParserT__5)
			}
			{
				p.SetState(48)
				p.ElseBlock()
			}

		}

	case 3:
		localctx = NewAssignmentContext(p, localctx)
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(51)
			p.Match(StepsParserID)
		}
		{
			p.SetState(52)
			p.Match(StepsParserT__0)
		}
		{
			p.SetState(53)
			p.expression(0)
		}
		{
			p.SetState(54)
			p.Match(StepsParserT__3)
		}

	}

	return localctx
}

// IFuncReturnsContext is an interface to support dynamic dispatch.
type IFuncReturnsContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Get_ID returns the _ID token.
	Get_ID() antlr.Token

	// Set_ID sets the _ID token.
	Set_ID(antlr.Token)

	// GetRetIds returns the retIds token list.
	GetRetIds() []antlr.Token

	// SetRetIds sets the retIds token list.
	SetRetIds([]antlr.Token)

	// IsFuncReturnsContext differentiates from other interfaces.
	IsFuncReturnsContext()
}

type FuncReturnsContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	_ID    antlr.Token
	retIds []antlr.Token
}

func NewEmptyFuncReturnsContext() *FuncReturnsContext {
	var p = new(FuncReturnsContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_funcReturns
	return p
}

func (*FuncReturnsContext) IsFuncReturnsContext() {}

func NewFuncReturnsContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FuncReturnsContext {
	var p = new(FuncReturnsContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_funcReturns

	return p
}

func (s *FuncReturnsContext) GetParser() antlr.Parser { return s.parser }

func (s *FuncReturnsContext) Get_ID() antlr.Token { return s._ID }

func (s *FuncReturnsContext) Set_ID(v antlr.Token) { s._ID = v }

func (s *FuncReturnsContext) GetRetIds() []antlr.Token { return s.retIds }

func (s *FuncReturnsContext) SetRetIds(v []antlr.Token) { s.retIds = v }

func (s *FuncReturnsContext) AllID() []antlr.TerminalNode {
	return s.GetTokens(StepsParserID)
}

func (s *FuncReturnsContext) ID(i int) antlr.TerminalNode {
	return s.GetToken(StepsParserID, i)
}

func (s *FuncReturnsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FuncReturnsContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FuncReturnsContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterFuncReturns(s)
	}
}

func (s *FuncReturnsContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitFuncReturns(s)
	}
}

func (p *StepsParser) FuncReturns() (localctx IFuncReturnsContext) {
	localctx = NewFuncReturnsContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, StepsParserRULE_funcReturns)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(58)

		var _m = p.Match(StepsParserID)

		localctx.(*FuncReturnsContext)._ID = _m
	}
	localctx.(*FuncReturnsContext).retIds = append(localctx.(*FuncReturnsContext).retIds, localctx.(*FuncReturnsContext)._ID)
	p.SetState(63)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == StepsParserT__6 {
		{
			p.SetState(59)
			p.Match(StepsParserT__6)
		}
		{
			p.SetState(60)

			var _m = p.Match(StepsParserID)

			localctx.(*FuncReturnsContext)._ID = _m
		}
		localctx.(*FuncReturnsContext).retIds = append(localctx.(*FuncReturnsContext).retIds, localctx.(*FuncReturnsContext)._ID)

		p.SetState(65)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IFuncParamsContext is an interface to support dynamic dispatch.
type IFuncParamsContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFuncParamsContext differentiates from other interfaces.
	IsFuncParamsContext()
}

type FuncParamsContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFuncParamsContext() *FuncParamsContext {
	var p = new(FuncParamsContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_funcParams
	return p
}

func (*FuncParamsContext) IsFuncParamsContext() {}

func NewFuncParamsContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FuncParamsContext {
	var p = new(FuncParamsContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_funcParams

	return p
}

func (s *FuncParamsContext) GetParser() antlr.Parser { return s.parser }

func (s *FuncParamsContext) AllFuncParam() []IFuncParamContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IFuncParamContext)(nil)).Elem())
	var tst = make([]IFuncParamContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IFuncParamContext)
		}
	}

	return tst
}

func (s *FuncParamsContext) FuncParam(i int) IFuncParamContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFuncParamContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IFuncParamContext)
}

func (s *FuncParamsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FuncParamsContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FuncParamsContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterFuncParams(s)
	}
}

func (s *FuncParamsContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitFuncParams(s)
	}
}

func (p *StepsParser) FuncParams() (localctx IFuncParamsContext) {
	localctx = NewFuncParamsContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, StepsParserRULE_funcParams)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(66)
		p.FuncParam()
	}
	p.SetState(71)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == StepsParserT__6 {
		{
			p.SetState(67)
			p.Match(StepsParserT__6)
		}
		{
			p.SetState(68)
			p.FuncParam()
		}

		p.SetState(73)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IFuncParamContext is an interface to support dynamic dispatch.
type IFuncParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFuncParamContext differentiates from other interfaces.
	IsFuncParamContext()
}

type FuncParamContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFuncParamContext() *FuncParamContext {
	var p = new(FuncParamContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_funcParam
	return p
}

func (*FuncParamContext) IsFuncParamContext() {}

func NewFuncParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FuncParamContext {
	var p = new(FuncParamContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_funcParam

	return p
}

func (s *FuncParamContext) GetParser() antlr.Parser { return s.parser }

func (s *FuncParamContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *FuncParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FuncParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FuncParamContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterFuncParam(s)
	}
}

func (s *FuncParamContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitFuncParam(s)
	}
}

func (p *StepsParser) FuncParam() (localctx IFuncParamContext) {
	localctx = NewFuncParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, StepsParserRULE_funcParam)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(74)
		p.expression(0)
	}

	return localctx
}

// IThenBlockContext is an interface to support dynamic dispatch.
type IThenBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsThenBlockContext differentiates from other interfaces.
	IsThenBlockContext()
}

type ThenBlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyThenBlockContext() *ThenBlockContext {
	var p = new(ThenBlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_thenBlock
	return p
}

func (*ThenBlockContext) IsThenBlockContext() {}

func NewThenBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ThenBlockContext {
	var p = new(ThenBlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_thenBlock

	return p
}

func (s *ThenBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *ThenBlockContext) Block() IBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBlockContext)
}

func (s *ThenBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ThenBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ThenBlockContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterThenBlock(s)
	}
}

func (s *ThenBlockContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitThenBlock(s)
	}
}

func (p *StepsParser) ThenBlock() (localctx IThenBlockContext) {
	localctx = NewThenBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, StepsParserRULE_thenBlock)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(76)
		p.Block()
	}

	return localctx
}

// IElseBlockContext is an interface to support dynamic dispatch.
type IElseBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsElseBlockContext differentiates from other interfaces.
	IsElseBlockContext()
}

type ElseBlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyElseBlockContext() *ElseBlockContext {
	var p = new(ElseBlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_elseBlock
	return p
}

func (*ElseBlockContext) IsElseBlockContext() {}

func NewElseBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ElseBlockContext {
	var p = new(ElseBlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_elseBlock

	return p
}

func (s *ElseBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *ElseBlockContext) Block() IBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBlockContext)
}

func (s *ElseBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ElseBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ElseBlockContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterElseBlock(s)
	}
}

func (s *ElseBlockContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitElseBlock(s)
	}
}

func (p *StepsParser) ElseBlock() (localctx IElseBlockContext) {
	localctx = NewElseBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, StepsParserRULE_elseBlock)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(78)
		p.Block()
	}

	return localctx
}

// IBlockContext is an interface to support dynamic dispatch.
type IBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBlockContext differentiates from other interfaces.
	IsBlockContext()
}

type BlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBlockContext() *BlockContext {
	var p = new(BlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_block
	return p
}

func (*BlockContext) IsBlockContext() {}

func NewBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BlockContext {
	var p = new(BlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_block

	return p
}

func (s *BlockContext) GetParser() antlr.Parser { return s.parser }

func (s *BlockContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *BlockContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *BlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BlockContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterBlock(s)
	}
}

func (s *BlockContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitBlock(s)
	}
}

func (p *StepsParser) Block() (localctx IBlockContext) {
	localctx = NewBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, StepsParserRULE_block)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(89)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case StepsParserT__7:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(80)
			p.Match(StepsParserT__7)
		}
		p.SetState(84)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == StepsParserT__4 || _la == StepsParserID {
			{
				p.SetState(81)
				p.Statement()
			}

			p.SetState(86)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}
		{
			p.SetState(87)
			p.Match(StepsParserT__8)
		}

	case StepsParserT__4, StepsParserID:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(88)
			p.Statement()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IExpressionContext is an interface to support dynamic dispatch.
type IExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsExpressionContext differentiates from other interfaces.
	IsExpressionContext()
}

type ExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExpressionContext() *ExpressionContext {
	var p = new(ExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_expression
	return p
}

func (*ExpressionContext) IsExpressionContext() {}

func NewExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExpressionContext {
	var p = new(ExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_expression

	return p
}

func (s *ExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *ExpressionContext) CopyFrom(ctx *ExpressionContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *ExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type IDExprContext struct {
	*ExpressionContext
}

func NewIDExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IDExprContext {
	var p = new(IDExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *IDExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IDExprContext) ID() antlr.TerminalNode {
	return s.GetToken(StepsParserID, 0)
}

func (s *IDExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterIDExpr(s)
	}
}

func (s *IDExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitIDExpr(s)
	}
}

type NumExprContext struct {
	*ExpressionContext
}

func NewNumExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NumExprContext {
	var p = new(NumExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *NumExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NumExprContext) NUMBER() antlr.TerminalNode {
	return s.GetToken(StepsParserNUMBER, 0)
}

func (s *NumExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterNumExpr(s)
	}
}

func (s *NumExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitNumExpr(s)
	}
}

type ParExprContext struct {
	*ExpressionContext
}

func NewParExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ParExprContext {
	var p = new(ParExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ParExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ParExprContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ParExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterParExpr(s)
	}
}

func (s *ParExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitParExpr(s)
	}
}

type CmpExprContext struct {
	*ExpressionContext
}

func NewCmpExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *CmpExprContext {
	var p = new(CmpExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *CmpExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CmpExprContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *CmpExprContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *CmpExprContext) OpCmp() IOpCmpContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpCmpContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpCmpContext)
}

func (s *CmpExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterCmpExpr(s)
	}
}

func (s *CmpExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitCmpExpr(s)
	}
}

type AndOrExprContext struct {
	*ExpressionContext
}

func NewAndOrExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AndOrExprContext {
	var p = new(AndOrExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *AndOrExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AndOrExprContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *AndOrExprContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *AndOrExprContext) OpAndOr() IOpAndOrContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpAndOrContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpAndOrContext)
}

func (s *AndOrExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterAndOrExpr(s)
	}
}

func (s *AndOrExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitAndOrExpr(s)
	}
}

type StrExprContext struct {
	*ExpressionContext
}

func NewStrExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *StrExprContext {
	var p = new(StrExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *StrExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StrExprContext) STRING() antlr.TerminalNode {
	return s.GetToken(StepsParserSTRING, 0)
}

func (s *StrExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterStrExpr(s)
	}
}

func (s *StrExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitStrExpr(s)
	}
}

type FuncExprContext struct {
	*ExpressionContext
}

func NewFuncExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *FuncExprContext {
	var p = new(FuncExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *FuncExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FuncExprContext) ID() antlr.TerminalNode {
	return s.GetToken(StepsParserID, 0)
}

func (s *FuncExprContext) FuncParams() IFuncParamsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFuncParamsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFuncParamsContext)
}

func (s *FuncExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterFuncExpr(s)
	}
}

func (s *FuncExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitFuncExpr(s)
	}
}

type MathExprContext struct {
	*ExpressionContext
}

func NewMathExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *MathExprContext {
	var p = new(MathExprContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *MathExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MathExprContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *MathExprContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *MathExprContext) OpMath() IOpMathContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOpMathContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOpMathContext)
}

func (s *MathExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterMathExpr(s)
	}
}

func (s *MathExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitMathExpr(s)
	}
}

func (p *StepsParser) Expression() (localctx IExpressionContext) {
	return p.expression(0)
}

func (p *StepsParser) expression(_p int) (localctx IExpressionContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()
	_parentState := p.GetState()
	localctx = NewExpressionContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExpressionContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 16
	p.EnterRecursionRule(localctx, 16, StepsParserRULE_expression, _p)
	var _la int

	defer func() {
		p.UnrollRecursionContexts(_parentctx)
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(105)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 10, p.GetParserRuleContext()) {
	case 1:
		localctx = NewFuncExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx

		{
			p.SetState(92)
			p.Match(StepsParserID)
		}
		{
			p.SetState(93)
			p.Match(StepsParserT__1)
		}
		p.SetState(95)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<StepsParserT__1)|(1<<StepsParserID)|(1<<StepsParserSTRING)|(1<<StepsParserNUMBER))) != 0 {
			{
				p.SetState(94)
				p.FuncParams()
			}

		}
		{
			p.SetState(97)
			p.Match(StepsParserT__2)
		}

	case 2:
		localctx = NewIDExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(98)
			p.Match(StepsParserID)
		}

	case 3:
		localctx = NewStrExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(99)
			p.Match(StepsParserSTRING)
		}

	case 4:
		localctx = NewNumExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(100)
			p.Match(StepsParserNUMBER)
		}

	case 5:
		localctx = NewParExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(101)
			p.Match(StepsParserT__1)
		}
		{
			p.SetState(102)
			p.expression(0)
		}
		{
			p.SetState(103)
			p.Match(StepsParserT__2)
		}

	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(121)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 12, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(119)
			p.GetErrorHandler().Sync(p)
			switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext()) {
			case 1:
				localctx = NewMathExprContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, StepsParserRULE_expression)
				p.SetState(107)

				if !(p.Precpred(p.GetParserRuleContext(), 3)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 3)", ""))
				}
				{
					p.SetState(108)
					p.OpMath()
				}
				{
					p.SetState(109)
					p.expression(4)
				}

			case 2:
				localctx = NewCmpExprContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, StepsParserRULE_expression)
				p.SetState(111)

				if !(p.Precpred(p.GetParserRuleContext(), 2)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 2)", ""))
				}
				{
					p.SetState(112)
					p.OpCmp()
				}
				{
					p.SetState(113)
					p.expression(3)
				}

			case 3:
				localctx = NewAndOrExprContext(p, NewExpressionContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, StepsParserRULE_expression)
				p.SetState(115)

				if !(p.Precpred(p.GetParserRuleContext(), 1)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 1)", ""))
				}
				{
					p.SetState(116)
					p.OpAndOr()
				}
				{
					p.SetState(117)
					p.expression(2)
				}

			}

		}
		p.SetState(123)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 12, p.GetParserRuleContext())
	}

	return localctx
}

// IOpMathContext is an interface to support dynamic dispatch.
type IOpMathContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOpMathContext differentiates from other interfaces.
	IsOpMathContext()
}

type OpMathContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOpMathContext() *OpMathContext {
	var p = new(OpMathContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_opMath
	return p
}

func (*OpMathContext) IsOpMathContext() {}

func NewOpMathContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OpMathContext {
	var p = new(OpMathContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_opMath

	return p
}

func (s *OpMathContext) GetParser() antlr.Parser { return s.parser }
func (s *OpMathContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OpMathContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OpMathContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterOpMath(s)
	}
}

func (s *OpMathContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitOpMath(s)
	}
}

func (p *StepsParser) OpMath() (localctx IOpMathContext) {
	localctx = NewOpMathContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, StepsParserRULE_opMath)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(124)
		_la = p.GetTokenStream().LA(1)

		if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<StepsParserT__9)|(1<<StepsParserT__10)|(1<<StepsParserT__11)|(1<<StepsParserT__12))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IOpCmpContext is an interface to support dynamic dispatch.
type IOpCmpContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOpCmpContext differentiates from other interfaces.
	IsOpCmpContext()
}

type OpCmpContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOpCmpContext() *OpCmpContext {
	var p = new(OpCmpContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_opCmp
	return p
}

func (*OpCmpContext) IsOpCmpContext() {}

func NewOpCmpContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OpCmpContext {
	var p = new(OpCmpContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_opCmp

	return p
}

func (s *OpCmpContext) GetParser() antlr.Parser { return s.parser }
func (s *OpCmpContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OpCmpContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OpCmpContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterOpCmp(s)
	}
}

func (s *OpCmpContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitOpCmp(s)
	}
}

func (p *StepsParser) OpCmp() (localctx IOpCmpContext) {
	localctx = NewOpCmpContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, StepsParserRULE_opCmp)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(126)
		_la = p.GetTokenStream().LA(1)

		if !(_la == StepsParserT__13 || _la == StepsParserT__14) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IOpAndOrContext is an interface to support dynamic dispatch.
type IOpAndOrContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOpAndOrContext differentiates from other interfaces.
	IsOpAndOrContext()
}

type OpAndOrContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOpAndOrContext() *OpAndOrContext {
	var p = new(OpAndOrContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = StepsParserRULE_opAndOr
	return p
}

func (*OpAndOrContext) IsOpAndOrContext() {}

func NewOpAndOrContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OpAndOrContext {
	var p = new(OpAndOrContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = StepsParserRULE_opAndOr

	return p
}

func (s *OpAndOrContext) GetParser() antlr.Parser { return s.parser }
func (s *OpAndOrContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OpAndOrContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OpAndOrContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.EnterOpAndOr(s)
	}
}

func (s *OpAndOrContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(StepsListener); ok {
		listenerT.ExitOpAndOr(s)
	}
}

func (p *StepsParser) OpAndOr() (localctx IOpAndOrContext) {
	localctx = NewOpAndOrContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, StepsParserRULE_opAndOr)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(128)
		_la = p.GetTokenStream().LA(1)

		if !(_la == StepsParserT__15 || _la == StepsParserT__16) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

func (p *StepsParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 8:
		var t *ExpressionContext = nil
		if localctx != nil {
			t = localctx.(*ExpressionContext)
		}
		return p.Expression_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *StepsParser) Expression_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 3)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 2)

	case 2:
		return p.Precpred(p.GetParserRuleContext(), 1)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
