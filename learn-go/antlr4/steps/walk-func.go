package steps

import (
	"fmt"

	"gitlab.com/stefarf/go/learn-go/antlr4/steps/parser"
)

func (sc *stepsCompiler) EnterFuncStmt(ctx *parser.FuncStmtContext) { sc.pushParams() }
func (sc *stepsCompiler) EnterFuncExpr(ctx *parser.FuncExprContext) { sc.pushParams() }

func (sc *stepsCompiler) ExitFuncParam(ctx *parser.FuncParamContext) {
	sc.incParams()
	sc.addMnemonic(Mnemonic{cmd: cmdPopAndPushParam})
	if DebugCompile {
		fmt.Println("pop and push param")
	}
}

func (sc *stepsCompiler) ExitFuncStmt(ctx *parser.FuncStmtContext) {
	params := sc.popParams()
	funcName := ctx.ID().GetText()

	var ids []string
	if ctx.FuncReturns() != nil {
		for _, id := range ctx.FuncReturns().GetRetIds() {
			ids = append(ids, id.GetText())
		}
	}

	sc.addMnemonic(Mnemonic{cmd: cmdPopParamsCallAndSave, i: params, s: funcName, a: ids})
	if DebugCompile {
		fmt.Printf("pop %d params, call %s() and save result to %v\n",
			params, funcName, ids)
	}
}

func (sc *stepsCompiler) ExitFuncExpr(ctx *parser.FuncExprContext) {
	params := sc.popParams()
	funcName := ctx.ID().GetText()

	sc.addMnemonic(Mnemonic{cmd: cmdPopParamsCallAndPush, i: params, s: funcName})
	if DebugCompile {
		fmt.Printf("pop %d params, call %s() and push result\n",
			params, funcName)
	}
}
