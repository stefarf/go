package steps

import (
	"fmt"

	"gitlab.com/stefarf/go/learn-go/antlr4/steps/parser"
)

func (sc *stepsCompiler) ExitAssignment(ctx *parser.AssignmentContext) {
	id := ctx.ID().GetText()
	sc.addMnemonic(Mnemonic{cmd: cmdPopAndAssign, s: id})
	if DebugCompile {
		fmt.Printf("pop and assign '%s'\n", id)
	}
}
