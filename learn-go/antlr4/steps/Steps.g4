grammar Steps;

// Tokens

ID: [_$a-zA-Z] [_$a-zA-Z0-9]* ;
STRING: '\'' ~['\r\n]* '\'' ;
NUMBER: [0-9.]+ ;
WHITESPACE: [ \r\n\t]+ -> skip;

// Rules

start: statement* EOF;

statement
    : (funcReturns '=' )? ID '(' funcParams? ')' ';' # FuncStmt
    | 'if' expression thenBlock ('else' elseBlock)?  # IfStmt
    | ID '=' expression ';'                          # Assignment
    ;

funcReturns: retIds+=ID ( ',' retIds+=ID)* ;
funcParams : funcParam ( ',' funcParam)* ;
funcParam  : expression ;

thenBlock: block ;
elseBlock: block ;
block    : '{' statement* '}' | statement ;

expression
    : ID '(' funcParams? ')'        # FuncExpr
    | ID                            # IDExpr
    | STRING                        # StrExpr
    | NUMBER                        # NumExpr
    | '(' expression ')'            # ParExpr
    | expression opMath expression  # MathExpr
    | expression opCmp expression   # CmpExpr
    | expression opAndOr expression # AndOrExpr
    ;

opMath : '+' | '-' | '*' | '/' ;
opCmp  : '==' | '!=' ;
opAndOr: 'and' | 'or' ;
