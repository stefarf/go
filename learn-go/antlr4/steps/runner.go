package steps

type (
	stepsRunner struct {
		hook  Hook
		stack []*string
		param []*string
	}
)

func (run *stepsRunner) pushStack(s *string) {
	run.stack = append(run.stack, s)
}

func (run *stepsRunner) popStack() *string {
	s := run.stack[len(run.stack)-1]
	run.stack = run.stack[:len(run.stack)-1]
	return s
}

func (run *stepsRunner) pushParam(s *string) {
	run.param = append(run.param, s)
}

func (run *stepsRunner) popParam() *string {
	s := run.param[len(run.param)-1]
	run.param = run.param[:len(run.param)-1]
	return s
}

func (run *stepsRunner) popParams(num int) (params []*string) {
	params = make([]*string, num)
	for i := num - 1; i >= 0; i-- {
		params[i] = run.popParam()
	}
	return
}
