package steps

import (
	"github.com/antlr/antlr4/runtime/Go/antlr"
)

func (sc *stepsCompiler) SyntaxError(
	recognizer antlr.Recognizer,
	offendingSymbol interface{},
	line,
	column int,
	msg string,
	e antlr.RecognitionException) {

	sc.syntaxError = true
}
func (sc *stepsCompiler) ReportAmbiguity(
	recognizer antlr.Parser,
	dfa *antlr.DFA,
	startIndex,
	stopIndex int,
	exact bool,
	ambigAlts *antlr.BitSet,
	configs antlr.ATNConfigSet) {

	// fmt.Println("report ambiguity")
}
func (sc *stepsCompiler) ReportAttemptingFullContext(
	recognizer antlr.Parser,
	dfa *antlr.DFA,
	startIndex,
	stopIndex int,
	conflictingAlts *antlr.BitSet,
	configs antlr.ATNConfigSet) {

	// fmt.Println("report attempting full context")
}
func (sc *stepsCompiler) ReportContextSensitivity(
	recognizer antlr.Parser,
	dfa *antlr.DFA,
	startIndex,
	stopIndex,
	prediction int,
	configs antlr.ATNConfigSet) {

	// fmt.Println("report context sensitivity")
}
