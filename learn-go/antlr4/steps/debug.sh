#!/usr/bin/env bash
mkdir debug
cd debug
cp ../Steps.g4 .
java -jar /usr/local/lib/antlr-4.7.1-complete.jar Steps.g4
javac *.java
java org.antlr.v4.gui.TestRig Steps start -gui <<EOF

if solution == '' solved = NULL;
else solved = NOW;
if problem != ''
    insertProblem('Non Project', id, problem, NOW, solution, solved);

a = f1(id1, 's1', f2(id2)) + (100.1 - f3());
b = c;
c = 's2';

print();
v1, v2 = query1(id1, 'hello');
if status == 'in progress' or (null(status, 'x') and x) {
    print('Then');
    if x do('For x');
    do(this, that);
} else print('Else');
do(something,'and others');
print();

EOF
cd ..
rm -rf debug
